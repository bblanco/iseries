﻿var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var seriesPepitoApi = require('./seriesPepitoApi');

app.engine('jade', require('jade').__express);
app.set('view engine', 'jade');
app.use(express.static(__dirname + '/public'));
// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser());


app.get('/', function(req, res){
  res.render('index', { msg: 'hola mundo' });
});


var router = express.Router(); 				// get an instance of the express Router

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
var seriesBD = [{code: 'erase', nombre: 'Érase una vez', url: 'http://once-upon-a-time-erase-una-vez.seriespepito.com/'},
{code: 'hundred', nombre: 'Los 100', url: 'http://the-hundred-the-100.seriespepito.com/'}];
router.get('/series', function(req, res) {
	res.json(seriesBD);	
});

function getSerieByCode(serieCode){
	var filtered = seriesBD.filter(function(element){
		return element.code === serieCode;
	});
	if (filtered.length) {
		return filtered[0];
	}
	return null;
}

router.get('/series/:serie_code', function(req, res) {
	var serieCode = req.params.serie_code;

	var serie = getSerieByCode(serieCode);
	var url = serie.url;

	seriesPepitoApi.getTemporadas(url, function(temporadas) {
		res.json(temporadas);	
	})
	
});

router.get('/series/:serie_code/:temporada/:capitulo', function(req, res) {
	var serieCode = req.params.serie_code;
	var serie = getSerieByCode(serieCode);
	var url = serie.url + 'temporada-' 
				+ req.params.temporada + '/capitulo-' + req.params.capitulo + '/';

	var capitulo = {url: url};
	seriesPepitoApi.getServidoresDisponiblesCapitulo(capitulo, function(servidores){
		res.json(servidores);	
 	});
});


router.post('/video', function(req, res) {
	var servidor = req.body;
	seriesPepitoApi.getVideoCapitulo(servidor, function(video){
		res.json(video);	
 	});
});


app.use('/api', router);



var port = Number(process.env.PORT || 3000);
var server = app.listen(port, function() {
    console.log('Listening on port %d', server.address().port);
});