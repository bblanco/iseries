var request = require('request');
var cheerio = require('cheerio');


//var urlVidspot = 'http://vidspot.net/bkc7p2i72rne';

function extracPostParams(url, callback) {
	request(url, function(error, response, html){
		var $ = cheerio.load(html);
		var params = {};
		$('form[name=F1] input[type=hidden]').each(function(index, element) {
			var $el = $(element);
			var name = $el.attr('name');
			var val = $el.val();
			params[name] = val;
		});
		callback(params);
	});	
};



module.exports = {
	getVideo : function(url, html, callback) {
		var $ = cheerio.load(html);
		var params = {};
		$('form[name=F1] input[type=hidden]').each(function(index, element) {
			var $el = $(element);
			var name = $el.attr('name');
			var val = $el.val();
			params[name] = val;
		});

		var postData = {form: params};
		request.post(
		    url,
		    postData,
		    function (error, response, html) {
		    		var $ = cheerio.load(html);
		    		var scriptPlayer = $($('#player_code > script')[3]).html();
		    		var indexOfParams = scriptPlayer.indexOf('{');
		    		var strOptions = scriptPlayer.substring(indexOfParams, scriptPlayer.length - 3);
		    		var options = JSON.parse(strOptions);
		    		callback({file: options.playlist[0].sources[0].file});
		    }
		);
	}
}
