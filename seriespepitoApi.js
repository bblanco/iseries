var request = require('request');
var cheerio = require('cheerio');
var md5 = require('MD5');
var parseVidspot = require('./parserVidspot');

//config request behaviour
var j = request.jar(); //manage cookies
var optionsDefault = {
    headers: {
				'Connection': 'keep-alive',
				'Cache-Control': 'max-age=0',
				'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
				'Accept-Encoding': 'gzip,deflate,sdch',
				'User-Agent': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.16 Safari/537.36',
				'Accept-Language': 'es-ES,es;q=0.8'
    },
    gzip: true,
    jar: j
};
var request = request.defaults(optionsDefault);


var replaceAll = function(source, str1, str2, ignore)
{
   return source.replace(new RegExp(str1.replace(/([\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g, function(c){return "\\" + c;}), "g"+(ignore?"i":"")), str2);
};

function _extend(target) {
    var sources = [].slice.call(arguments, 1);
    sources.forEach(function (source) {
        for (var prop in source) {
            target[prop] = source[prop];
        }
    });
    return target;
}


function getTemporadas(url, callback) {
	request(url, function(error, response, html){
			var $ = cheerio.load(html);
			var temporadas = [];
			$('.accordion-group').each(function(index, element){
					console.log("temp");
					var temporada = parseTemporada($(element));
					temporadas.push(temporada);
			});

			callback(temporadas);
		});	
};


function parseTemporada($el) {
	var temporada = {};
	temporada.descripcion = $el.find('.accordion-heading').text().trim();
	temporada.capitulos = extractCapitulosTemp($el);
	return temporada;
};

function extractCapitulosTemp($el) {
	var capitulos = [];

	$el.find('.nav_lista_capitulos a').each(function(index, element){
			var $element = cheerio(element);
			var capitulo = parseCapitulo($element);
			capitulos.push(capitulo);
	});
	return capitulos;
};

function parseCapitulo($el) {
	var capitulo = {};
	capitulo.descripcion = $el.attr('title').trim();
	capitulo.url = $el.attr('href');
	capitulo.idioma = {};
	capitulo.idioma.castellano = $el.find('.flag_0').length > 0;
	return capitulo;
};


function getServidoresDisponiblesCapitulo(capitulo, callback) {
		return getServidoresCapitulo(capitulo.url, callback);
};

function getServidoresCapitulo(url, callback) {
	var options = {url: url, jar:request.jar()};
	request(options, function(error, response, html){
		var $ = cheerio.load(html);
		var servidores = [];

		$('table.tenlaces tr').each(function(index, element) {
			var $el = $(element);
			var servidor = parseServidor($el);
			servidor.urlcapitulo = url;
			if (disponiblePlataforma(servidor.plataforma)) {
				servidores.push(servidor);
			}
			
		});
		callback(servidores);
		/*var i = 0;
		var len = servidores.length;
		var cargados = 0;
		for (;i < len; i++) {
			var servidor = servidores[i];
			getVideoCapitulo(servidor, function(video) {
				servidor.video = video;
				cargados++;
				if (cargados >= len) {
					callback(servidores);
				}
			});
		}*/
		
		
	});	
};


function parseServidor($el) {
	var servidor = {};
	servidor.castellano = $el.find('.tdidioma .flag_0').length > 0;
	servidor.plataforma = cheerio($el.find('.tdservidor img')[0]).attr('alt');
	servidor.url = cheerio($el.find('.tdenlace a')[0]).attr('href');
	return servidor
};



function getVideoCapitulo(servidor, callback) {
	if (!disponiblePlataforma(servidor.plataforma)) {
		throw "La plataforma " + servidor.plataforma + ' no esta disponible';
	}

	extractPageVideoPlataform(servidor, function(urlPlataforma, html){
		var parserPlataforma = getParserPlataforma(servidor.plataforma);
		parserPlataforma.getVideo(urlPlataforma, html, function(video) {
			callback(video);
		});
		
	});
};

function extractPageVideoPlataform(servidor, callback) {
		var options = _extend({}, optionsDefault);
		options.headers['Referer'] = servidor.urlcapitulo;
		options.url = servidor.url;
		var j  = request.jar();
		options.jar = j;
		request(options, function(error, response, html) {
			var $ = cheerio.load(html);
			var linkvideo = extractEnlaceVideo(servidor.url, $, j);

			var originalUrl = options.url;
			options.url = linkvideo;
			options.Referer = originalUrl;
			request(linkvideo, function(error, response, html){
				var urlPlataformVideo = response.request.href;
				callback(urlPlataformVideo, html);
			});
		});
};

function extractEnlaceVideo(url, $, j) {
		var cookies = j.getCookies(url); 
		var keyVideo = '';
		for (var i = 0; i < cookies.length; i++) {
			if (cookies[0].key.length == 32) {
				keyVideo = cookies[0].value;
			}
			
		}

		var play = $($('tr.trenlacedetalle td')[1]);

		var linkVideo = play.find('a[class$=' + md5(keyVideo) + ']').attr('href');
		
		//extrae logica de la pagina
		var scriptPage =$($('script')[1]).html();
		var idx = scriptPage.indexOf('=/www.enlacespepito.com');
		var linesString = scriptPage.substring(idx)
																.split('\t').join('')
																.split('\r').join('')
																.split(' ')
																.join('').split('\n');
		var cleanline = linesString[2].substring(0, linesString[2].indexOf('$'))														
		var varname = cleanline.substring(0,1);
		cleanline = replaceAll(cleanline, varname + '[', 'k[', 'g');
		var expr =/www.enlacespepito.com\/([^\.]*).html/ig;			
		var k=(expr.exec(linkVideo))[1].split('');
		eval(cleanline);
		
		linkVideo = 'http://www.enlacespepito.com/'+k.join('')+'.html';
		return linkVideo;
};

function disponiblePlataforma(plataforma) {
	return plataforma === 'vidspot';
};

function getParserPlataforma(plataforma) {
	if (plataforma === 'vidspot') {
		return parseVidspot;
	}
};




module.exports = {
	getTemporadas: function(url, callback) {
		getTemporadas(url, callback);
	},
	getServidoresDisponiblesCapitulo: function(capitulo, callback){
		var self = this;
		//j.removeCookie('.enlacespepito.com');
		//j.removeCookie('.seriespepito.com');
		//request.jar();
		getServidoresDisponiblesCapitulo(capitulo, function(servidores) {
				//self.getVideoCapitulo(servidores[2], function(video) {
				//	servidores[2].video = video;
					callback(servidores);
				//})
		}); 
	},
	getVideoCapitulo: function(servidor, callback) {
		getVideoCapitulo(servidor, callback);
	},
	getAll: function (callback) {
		var self = this;
		var capitulo = {url: 'http://once-upon-a-time-erase-una-vez.seriespepito.com/temporada-3/capitulo-0/'};
		this.getServidoresDisponiblesCapitulo(capitulo, function(servidores) {
			self.getVideoCapitulo(servidores[0], function(video) {
				callback(video);
			})
		});
	}
}