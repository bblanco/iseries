'use strict';

angular.module('iseries')
	.controller('ListadoSeriesCtrl', ['$scope', '$http', 'listadoSeries', 
		function($scope, $http, listadoSeries) {
			$scope.series = listadoSeries ? listadoSeries.data : [];
			

		}
	])
	.controller('SeriesCtrl', ['$scope', 'temporadas', '$state',
		function($scope, temporadas, $state) {
			$scope.temporadas = temporadas ? temporadas.data : [];
			$scope.state = $state;
		}
	])
	.controller('TemporadaCtrl', ['$scope', 'temporada',
		function($scope, temporada) {
			$scope.capitulos = temporada.capitulos;
		}
	])
	.controller('CapituloCtrl', ['$scope','$http', 'servidores', 'capitulo',
		function($scope, $http, servidores, capitulo) {
			$scope.servidores = servidores.data;
			$scope.capitulo = capitulo;
		

			$scope.obtenerVideo = function(servidor) {
				if (!servidor.video) {
					$http.post('/api/video', servidor).success(function(data){
						servidor.video = data.file;
						servidor.showVideo = true;
					});
				}
				
			};
		}
	]);