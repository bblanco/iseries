'use strict';

(function() {

	var app = angular.module('iseries', ['ionic','ui.router']);

	app.run()
		.config(['$stateProvider', '$urlRouterProvider',
			function($stateProvider, $urlRouterProvider) {
				$urlRouterProvider.otherwise('/series');

				$stateProvider.state('series', {
					url: '/series',
					templateUrl: 'views/index.html',
					controller: 'ListadoSeriesCtrl',
					resolve: {
						listadoSeries: function($http){
	            return $http({method: 'GET', url: '/api/series'});
         		}
					}
				});


				$stateProvider.state('detalle', {
					url: "/:seriecode",
					templateUrl: 'views/detalleSerie.html',
					controller: 'SeriesCtrl',
					resolve: {
						temporadas: ['$http', '$stateParams', function($http, $stateParams){
							 return $http({method: 'GET', url: '/api/series/' + $stateParams.seriecode});
			      }]
			    }
				});

				$stateProvider.state('detalle.temporada', {
					url: "/:numtemporada",
					templateUrl: 'views/capitulos.html',
					controller: 'TemporadaCtrl',
					resolve: {
						temporada: ['$stateParams', 'temporadas', function($stateParams, temporadas){
							var listTemp = temporadas.data;
							var ordinalTemp = parseInt($stateParams.numtemporada, 10) - 1;
							return listTemp[ordinalTemp];
			      }]
			    }
				});

				$stateProvider.state('detalle.temporada.capitulo', {
					url: "/:capitulo",
					templateUrl: 'views/servidoresCapitulo.html',
					controller: 'CapituloCtrl',
					resolve: {
						capitulo: ['$stateParams', 'temporada', function($stateParams, temporada){
							var listCapitulos = temporada.capitulos;
							var ordinalCap = parseInt($stateParams.capitulo, 10) - 1;
							return listCapitulos[ordinalCap];
			      }],
			      servidores: ['$stateParams', '$http','capitulo', function($stateParams, $http, capitulo){
							var ordinalCapSerie = capitulo.url.substring(capitulo.url.length-2, capitulo.url.length-1);
							var url = '/api/series/' + $stateParams.seriecode  
												+ '/' + $stateParams.numtemporada 
												+ '/' + ordinalCapSerie;
							return $http({method: 'GET', url: url});
			      }]
			    }
				});


			}
		]);

})();



delete window.document.referrer;
        window.document.__defineGetter__('referrer', function () {
            return "";
        });