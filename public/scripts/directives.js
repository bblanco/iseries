'use strict';

angular.module('iseries')
	.directive('videoplayer', function() {
    return {
     	link: function(scope, element, attrs) {
      	var videlem = document.createElement("video");
      	var sourceMP4 = document.createElement("source"); 
      	sourceMP4.type = "video/mp4";
      	sourceMP4.src = scope.servidor.video;
      	videlem.appendChild(sourceMP4);
      	element.append(videlem);
      }



    };
  });